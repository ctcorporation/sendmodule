﻿using NodeData.Models;
using System.Collections.Generic;

namespace SendModule
{
    public interface IFtpHelper
    {
        string ErrString { get; set; }
        string FileName { get; }
        Ivw_CustomerProfile Profile { get; set; }
        string ReceivePath { get; set; }
        string WinSCPLocation { get; set; }

        List<string> FTPReceive(string fileMask);
        string FtpSend();
        string FtpSend(string file, string server, string folder, string user, string password);
    }
}