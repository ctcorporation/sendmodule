﻿using System;
using System.Collections.Generic;
using System.IO;
using WinSCP;

namespace SendModule
{
    public class FtpHelper : IFtpHelper
    {
        #region Members
        private string _errString;
        private string _winScpLocation;
        private SessionOptions _sessionOptions;
        private string _fileName;
        private int _retryCount;
        private int _ftpPort = 21;
        #endregion

        #region Properties
        public string FileName
        {
            get
            { return _fileName; }
            set { _fileName = value; }
        }

        public string WinSCPLocation
        {
            get
            {
                return _winScpLocation;
            }
            set
            {
                _winScpLocation = value;
            }

        }
        public string ReceivePath
        {
            get; set;
        }


        public string sshfingerPrint
        {
            get; set;
        }

        public string PrivateKeyPath
        {
            get; set;
        }

        public string ErrString
        {
            get
            { return _errString; }
            set
            { _errString = value; }
        }

        public SessionOptions FTPsessionOptions
        {
            get
            { return _sessionOptions; }
            set
            { _sessionOptions = value; }

        }

        public int FtpPort
        {
            get
            {
                return _ftpPort;
            }
            set
            {
                _ftpPort = value;
            }
        }

        public int RetryCount
        {
            get
            {
                return _retryCount;
            }
            set
            {
                _retryCount = value;
            }
        }
        #endregion

        #region Constructors


        public FtpHelper(string winSCPLocation)
        {

            _winScpLocation = winSCPLocation;
        }

        public FtpHelper(string filePath, string winSCPLocation)
        {

            ReceivePath = filePath;
            _winScpLocation = winSCPLocation;
        }

        #endregion

        #region Methods
        public string FtpSend(string sendPath)
        {
            string Result = string.Empty;
            try
            {

                if (FTPsessionOptions == null)
                {
                    return "Error: Missing FTP Server Settings";
                }
                TransferOperationResult tr;

                using (Session session = new Session())
                {
                    session.ExecutablePath = WinSCPLocation;
                    session.Open(FTPsessionOptions);
                    if (session.Opened)
                    {

                        TransferOptions tOptions = new TransferOptions();
                        tOptions.OverwriteMode = OverwriteMode.Overwrite;
                        tOptions.ResumeSupport.State = TransferResumeSupportState.Off;
                        sendPath = string.IsNullOrEmpty(sendPath) ? "" : sendPath + "/";
                        tr = session.PutFiles(FileName, "/" + sendPath + Path.GetFileName(FileName), true, tOptions);
                        if (tr.IsSuccess)
                        {
                            Result = "File Sent Ok";
                            try
                            {
                                File.Delete(FileName);
                            }
                            catch (Exception ex)
                            {
                                Result = "File Sent Ok. Delete Error: " + ex.Message;
                            }
                        }
                        else
                        {
                            Result += "Failed: Transfer Result is invalid: ";
                        }
                    }
                    else
                    {
                        Result = "Failed to Open FTP Session" + Environment.NewLine;
                        Result += "FTP Server: " + FTPsessionOptions.HostName + Environment.NewLine;
                        Result += "Username: " + FTPsessionOptions.UserName + Environment.NewLine;
                        Result += "Port: " + FTPsessionOptions.PortNumber + Environment.NewLine;

                    }

                }

            }
            catch (TimeoutException ex)
            {
                Result += "Failed: " + ex.GetType().Name + " " + ex.Message;
            }
            catch (SessionRemoteException ex)
            {
                Result += "Failed: " + ex.GetType().Name + " " + ex.Message;
            }
            catch (SessionLocalException ex)
            {
                Result += "Failed: " + ex.GetType().Name + " " + ex.Message;
            }
            catch (Exception ex)
            {
                Result += "Failed: " + ex.GetType().Name + " " + ex.Message;
            }

            return Result;
        }

        public SessionOptions BuildSessionOptions(string host, int port, string username, string password, bool ssl)
        {
            _ftpPort = port;
            var so = new SessionOptions
            {
                HostName = host,
                PortNumber = port,
                UserName = username,
                Password = password,
                Protocol = ssl ? Protocol.Sftp : Protocol.Ftp,


            };
            if (ssl)
            {
                so.Protocol = WinSCP.Protocol.Sftp;
                so.SshHostKeyFingerprint = sshfingerPrint;
                so.SshPrivateKeyPath = PrivateKeyPath;
            }
            else
            {
                so.Protocol = WinSCP.Protocol.Ftp;
            }
            FTPsessionOptions = so;
            return so;
        }
        #endregion


        public string FtpSend(string file, string server, int port, string folder, string user, string password)
        {
            string Result = string.Empty;
            if (_retryCount > 0)
            {

            }
            try
            {
                SessionOptions sessionOptions = BuildSessionOptions(

                server,
                //_ftpPort,
                port,
                user,
                password,
                false

                );

                //try
                //{
                TransferOperationResult tr;
                using (Session session = new Session())
                {

                    session.ExecutablePath = _winScpLocation;
                    session.Open(sessionOptions);
                    if (session.Opened)
                    {
                        //NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Connected to " + custProfile[0].P_server + ". Sending file  :" + xmlfile + Environment.NewLine + " TO " + sessionOptions.HostName + "/" + custProfile[0].P_path + "/" + Path.GetFileName(xmlfile));
                        TransferOptions tOptions = new TransferOptions();
                        tOptions.OverwriteMode = OverwriteMode.Overwrite;
                        tOptions.ResumeSupport.State = TransferResumeSupportState.Off;

                        if (string.IsNullOrEmpty(folder))
                        {
                            folder = "";
                        }
                        else
                        {
                            folder = folder + "/";
                        }
                        tr = session.PutFiles(file, "/" + folder + Path.GetFileName(file), true, tOptions);
                        //NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Results: ");                        
                        Result += "FTP Server: " + sessionOptions.HostName + Environment.NewLine;
                        Result += "Username: " + sessionOptions.UserName + Environment.NewLine;
                        Result += "Port: " + sessionOptions.PortNumber + Environment.NewLine;
                        if (tr.IsSuccess)
                        {
                            Result = "File Sent Ok";
                            try
                            {
                                File.Delete(file);
                            }
                            catch (Exception ex)
                            {
                                Result = "File Sent Ok. Delete Error: " + ex.Message;
                            }

                        }
                        else
                        {
                            switch (_retryCount)
                            {
                                case -1:
                                    Result = "Failed: Transfer Result is invalid and Retries exceeded: ";
                                    break;
                                case 0:
                                    Result = "Failed: Transfer Result is invalid. Consider a retry count: ";
                                    break;
                                default:
                                    var res = FtpSend(file, server, port, folder, user, password);
                                    while (res != "File Sent Ok" || _retryCount > 0)
                                    {
                                        _retryCount--;
                                        if (_retryCount == 0)
                                        {
                                            _retryCount = -1;
                                        }
                                        res = FtpSend(file, server, port, folder, user, password);
                                    };
                                    break;
                            }


                        }

                    }
                    else
                    {
                        Result = "Failed to Open FTP Session" + Environment.NewLine;
                        Result += "FTP Server: " + sessionOptions.HostName + Environment.NewLine;
                        Result += "Username: " + sessionOptions.UserName + Environment.NewLine;
                        Result += "Port: " + sessionOptions.PortNumber + Environment.NewLine;
                    }
                }

            }
            catch (TimeoutException ex)
            {
                Result += "Failed: " + ex.GetType().Name + " " + ex.Message;
            }
            catch (SessionRemoteException ex)
            {
                Result += "Failed: " + ex.GetType().Name + " " + ex.Message;
            }
            catch (SessionLocalException ex)
            {
                Result += "Failed: " + ex.GetType().Name + " " + ex.Message;
            }
            catch (Exception ex)
            {
                Result += "Failed: " + ex.GetType().Name + " " + ex.Message + " Inner Exception" + ex.InnerException;
            }

            return Result;
        }

        public List<string> FTPReceive(string fileMask, string server, string port, string folder, string user, string password, bool ssl)
        {
            List<string> result = new List<string>();
            var sessionOptions = BuildSessionOptions(server, int.Parse(port), user, password, ssl);

            TransferOptions wsto = new TransferOptions
            {
                FileMask = fileMask,
                TransferMode = TransferMode.Automatic,
                OverwriteMode = OverwriteMode.Overwrite
            };
            string remoteFolder = string.Format("/{0}/*", folder);

            using (Session wcp = new Session())
            {
                try
                {
                    wcp.ExecutablePath = WinSCPLocation;
                    wcp.Open(FTPsessionOptions);
                    TransferOperationResult tResult;

                    tResult = wcp.GetFiles(remoteFolder, !ReceivePath.EndsWith("\\") ? ReceivePath += "\\" : ReceivePath, true, wsto);
                    tResult.Check();

                    foreach (TransferEventArgs transferred in tResult.Transfers)
                    {
                        result.Add(transferred.Destination);

                    }

                }
                catch (TimeoutException ex)
                {
                    _errString += "FTP Session Timed out. " + ex.GetType().Name + ":" + ex.Message + " " + FTPsessionOptions.HostName + "/" + folder + Environment.NewLine;
                    return null;
                }
                catch (Exception ex)
                {
                    _errString += "FTP Receive Error " + ex.GetType().Name + " " + FTPsessionOptions.HostName + "/" + folder + Environment.NewLine
                         + ex.Message;
                    return null;
                }
            }
            _errString = string.Empty;
            return result;
        }

        public List<string> FTPReceive(string fileMask, string receivePath)
        {
            List<string> result = new List<string>();
            //var sessionOptions = BuildSessionOptions(Profile.P_SERVER, int.Parse(Profile.P_PORT), Profile.P_USERNAME, Profile.P_PASSWORD, Profile.P_SSL == "Y" ? true : false);

            TransferOptions wsto = new TransferOptions
            {
                FileMask = fileMask,
                TransferMode = TransferMode.Automatic,
                OverwriteMode = OverwriteMode.Overwrite
            };
            string remoteFolder = string.Format("/{0}/*", receivePath.Trim());

            using (Session wcp = new Session())
            {
                wcp.ExecutablePath = WinSCPLocation;
                wcp.Open(FTPsessionOptions);
                TransferOperationResult tResult;
                try
                {
                    tResult = wcp.GetFiles(remoteFolder, !ReceivePath.EndsWith("\\") ? ReceivePath += "\\" : ReceivePath, true, wsto);
                    tResult.Check();

                    foreach (TransferEventArgs transferred in tResult.Transfers)
                    {
                        result.Add(transferred.Destination);

                    }

                }
                catch (Exception ex)
                {
                    _errString += "FTP Receive Error " + ex.GetType().Name + " " + FTPsessionOptions.HostName + "/" + receivePath + Environment.NewLine
                         + ex.Message;
                    return null;
                }
            }
            _errString = string.Empty;
            return result;
        }
    }
}
