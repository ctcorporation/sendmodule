﻿using System.Collections.Generic;
using WinSCP;

namespace SendModule
{
    public interface IFtpHelper
    {
        int RetryCount { get; set; }
        string ErrString { get; set; }
        string FileName { get; set; }
        string ReceivePath { get; set; }
        string WinSCPLocation { get; set; }
        int FtpPort { get; set; }
        SessionOptions FTPsessionOptions
        {
            get; set;
        }
        string sshfingerPrint
        {
            get; set;
        }
        string PrivateKeyPath
        {
            get; set;
        }

        SessionOptions BuildSessionOptions(string host, int port, string username, string password, bool ssl);
        List<string> FTPReceive(string fileMask, string receivePath);
        List<string> FTPReceive(string fileMask, string server, string port, string folder, string user, string password, bool ssl);

        string FtpSend(string sendPath);
        string FtpSend(string file, string server, int port, string folder, string user, string password);
    }
}