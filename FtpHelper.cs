﻿
using NodeData.Models;
using System;
using System.Collections.Generic;
using System.IO;
using WinSCP;

namespace SendModule
{
    public class FtpHelper : IFtpHelper
    {
        #region Members
        private string _errString;
        private string _winScpLocation;
        #endregion

        #region Properties
        public Ivw_CustomerProfile Profile { get; set; }
        public string FileName { get; }

        public string WinSCPLocation { get; set; }
        public string ReceivePath
        {
            get; set;
        }

        public string ErrString
        {
            get
            { return _errString; }
            set
            { _errString = value; }
        }
        #endregion

        #region Constructors

        public FtpHelper(string fileToSend)
        {

        }

        public FtpHelper(Ivw_CustomerProfile profile, string winSCPLocation)
        {
            Profile = profile;
            WinSCPLocation = winSCPLocation;
        }

        public FtpHelper(Ivw_CustomerProfile profile, string filePath, string WinSCPLocation)
        {
            Profile = profile;
            ReceivePath = filePath;
        }
        public FtpHelper(string fileToSend, Ivw_CustomerProfile profile, string WinSCPLocation)
        {
            Profile = profile;
            FileName = fileToSend;
        }

        #endregion

        #region Methods
        public string FtpSend()
        {
            string Result = string.Empty;
            try
            {

                SessionOptions sessionOptions = BuildSessionOptions(Profile.P_SERVER.Trim(),
                    string.IsNullOrEmpty(Profile.P_PORT) ? 21 : int.Parse(Profile.P_PORT),
                    Profile.P_USERNAME.Trim(),
                    Profile.P_PASSWORD.Trim(),
                    Profile.P_SSL == "Y" ? true : false);
                TransferOperationResult tr;

                using (Session session = new Session())
                {
                    session.ExecutablePath = WinSCPLocation;
                    session.Open(sessionOptions);
                    if (session.Opened)
                    {

                        TransferOptions tOptions = new TransferOptions();
                        tOptions.OverwriteMode = OverwriteMode.Overwrite;
                        tOptions.ResumeSupport.State = TransferResumeSupportState.Off;
                        Profile.P_PATH = string.IsNullOrEmpty(Profile.P_PATH) ? "" : Profile.P_PATH + "/";
                        tr = session.PutFiles(FileName, "/" + Profile.P_PATH + Path.GetFileName(FileName), true, tOptions);
                        if (tr.IsSuccess)
                        {
                            Result = "File Sent Ok";
                            try
                            {
                                File.Delete(FileName);
                            }
                            catch (Exception ex)
                            {
                                Result = "File Sent Ok. Delete Error: " + ex.Message;
                            }
                        }
                        else
                        {
                            Result += "Failed: Transfer Result is invalid: ";
                        }
                    }
                    else
                    {
                        Result = "Failed to Open FTP Session" + Environment.NewLine;
                        Result += "FTP Server: " + sessionOptions.HostName + Environment.NewLine;
                        Result += "Username: " + sessionOptions.UserName + Environment.NewLine;
                        Result += "Port: " + sessionOptions.PortNumber + Environment.NewLine;

                    }

                }

            }
            catch (TimeoutException ex)
            {
                Result += "Failed: " + ex.GetType().Name + " " + ex.Message;
            }
            catch (SessionRemoteException ex)
            {
                Result += "Failed: " + ex.GetType().Name + " " + ex.Message;
            }
            catch (SessionLocalException ex)
            {
                Result += "Failed: " + ex.GetType().Name + " " + ex.Message;
            }
            catch (Exception ex)
            {
                Result += "Failed: " + ex.GetType().Name + " " + ex.Message;
            }

            return Result;
        }

        private SessionOptions BuildSessionOptions(string host, int port, string username, string password, bool ssl)
        {
            var so = new SessionOptions
            {
                HostName = host,
                PortNumber = port,
                UserName = username,
                Password = password,
                Protocol = ssl ? Protocol.Sftp : Protocol.Ftp,


            };
            if (ssl)
            {
                so.Protocol = WinSCP.Protocol.Sftp;
                so.SshHostKeyFingerprint = Profile.P_EMAILADDRESS;
                so.SshPrivateKeyPath = Profile.P_SUBJECT;
            }
            else
            {
                so.Protocol = WinSCP.Protocol.Ftp;
            }
            return so;
        }
        #endregion


        public string FtpSend(string file, string server, string folder, string user, string password)
        {
            string Result = string.Empty;
            try
            {
                SessionOptions sessionOptions = BuildSessionOptions(

                server,
                21,
                user,
                password,
                false

                );

                //try
                //{
                TransferOperationResult tr;
                using (Session session = new Session())
                {
                    session.ExecutablePath = WinSCPLocation;
                    session.Open(sessionOptions);
                    if (session.Opened)
                    {
                        //NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Connected to " + custProfile[0].P_server + ". Sending file  :" + xmlfile + Environment.NewLine + " TO " + sessionOptions.HostName + "/" + custProfile[0].P_path + "/" + Path.GetFileName(xmlfile));
                        TransferOptions tOptions = new TransferOptions();
                        tOptions.OverwriteMode = OverwriteMode.Overwrite;
                        tOptions.ResumeSupport.State = TransferResumeSupportState.Off;

                        if (string.IsNullOrEmpty(folder))
                        {
                            folder = "";
                        }
                        else
                        {
                            folder = folder + "/";
                        }
                        tr = session.PutFiles(file, "/" + folder + Path.GetFileName(file), true, tOptions);
                        //NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Results: ");                        
                        Result += "FTP Server: " + sessionOptions.HostName + Environment.NewLine;
                        Result += "Username: " + sessionOptions.UserName + Environment.NewLine;
                        Result += "Port: " + sessionOptions.PortNumber + Environment.NewLine;
                        if (tr.IsSuccess)
                        {
                            Result = "File Sent Ok";
                            try
                            {
                                File.Delete(file);
                            }
                            catch (Exception ex)
                            {
                                Result = "File Sent Ok. Delete Error: " + ex.Message;
                            }

                        }
                        else
                        {
                            Result = "Failed: Transfer Result is invalid: ";
                        }

                    }
                    else
                    {
                        Result = "Failed to Open FTP Session" + Environment.NewLine;
                        Result += "FTP Server: " + sessionOptions.HostName + Environment.NewLine;
                        Result += "Username: " + sessionOptions.UserName + Environment.NewLine;
                        Result += "Port: " + sessionOptions.PortNumber + Environment.NewLine;
                    }
                }

            }
            catch (TimeoutException ex)
            {
                Result += "Failed: " + ex.GetType().Name + " " + ex.Message;
            }
            catch (SessionRemoteException ex)
            {
                Result += "Failed: " + ex.GetType().Name + " " + ex.Message;
            }
            catch (SessionLocalException ex)
            {
                Result += "Failed: " + ex.GetType().Name + " " + ex.Message;
            }
            catch (Exception ex)
            {
                Result += "Failed: " + ex.GetType().Name + " " + ex.Message;
            }

            return Result;
        }

        public List<string> FTPReceive(string fileMask)
        {
            List<string> result = new List<string>();
            var sessionOptions = BuildSessionOptions(Profile.P_SERVER, int.Parse(Profile.P_PORT), Profile.P_USERNAME, Profile.P_PASSWORD, Profile.P_SSL == "Y" ? true : false);
            TransferOptions wsto = new TransferOptions
            {
                FileMask = fileMask,
                TransferMode = TransferMode.Automatic,
                OverwriteMode = OverwriteMode.Overwrite
            };
            string remoteFolder = string.Format("/{0}/*", Profile.P_PATH.Trim());

            using (Session wcp = new Session())
            {
                wcp.ExecutablePath = WinSCPLocation;
                wcp.Open(sessionOptions);
                TransferOperationResult tResult;
                try
                {
                    tResult = wcp.GetFiles(remoteFolder, !ReceivePath.EndsWith("\\") ? ReceivePath += "\\" : ReceivePath, true, wsto);
                    tResult.Check();

                    foreach (TransferEventArgs transferred in tResult.Transfers)
                    {
                        result.Add(transferred.Destination);

                    }

                }
                catch (Exception ex)
                {
                    _errString += "FTP Receive Error " + ex.GetType().Name + " " + Profile.P_SERVER + "/" + Profile.P_PATH + Environment.NewLine
                         + ex.Message;
                    return null;
                }
            }
            _errString = string.Empty;
            return result;
        }
    }
}
